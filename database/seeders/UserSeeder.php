<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Main user
        User::factory()->create([
            'name' => 'admin',
            'email' => 'a@a.a',
            'password' => Hash::make('admin123')
        ]);

        User::factory(10)->create();
    }
}
