<?php

namespace App\Http\Requests;

use App\Enums\StatusEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'lastname' => 'required|string|min:2|max:30',
            'firstname' => 'required|string|min:2|max:30',
            'image' => 'nullable|mimes:jpeg,jpg,png,svg,bmp|min:1|max:500',
            'status' => [
                'required',
                Rule::in([StatusEnum::ACTIF, StatusEnum::INACTIF, StatusEnum::EN_ATTENTE]),
            ],
        ];
    }

    public function messages(): array
    {
        return [
            'lastname.required' => 'The lastname is required',
            'lastname.string' => 'The lastname must be of type string',
            'lastname.min' => 'The lastname must be at least :min characters long',
            'lastname.max' => 'The lastname may not be greater than :max characters',
            'firstname.required' => 'The firstname is required',
            'firstname.string' => 'The firstname must be of type string',
            'firstname.min' => 'The firstname must be at least :min characters long',
            'firstname.max' => 'The firstname may not be greater than :max characters',
            'image.mimes' => 'The image must be of type jpeg,jpg,png,svg,bmp',
            'image.min' => 'The image must be at least :min kB long',
            'image.max' => 'The image may not be greater than :max kB',
            'status.required' => 'The status is required',
            'status.in' => 'The status must be Actif, Inactif or En attente',
        ];
    }
}
