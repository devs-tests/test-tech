<?php

namespace App\Http\Dto;

class CreateTokenDto {

    public function __construct(
        public string $email,
        public string $password
    )
    {
        
    }
}