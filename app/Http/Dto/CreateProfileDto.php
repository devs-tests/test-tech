<?php

namespace App\Http\Dto;

use Illuminate\Http\UploadedFile;

class CreateProfileDto {

    public function __construct(
        public string $lastname,
        public string $firstname,
        public ?UploadedFile $image,
        public string $status
    )
    {

    }
}