<?php

namespace App\Services;

use Illuminate\Http\UploadedFile;

class ImageService
{
    public static function storeImage(?UploadedFile $image)
    {
        $imageName = null;
            
        if ($image) {
            $imageName = date('Ymd') . '-' . uniqid() . '.' . $image->getClientOriginalExtension();
            
            // Enregistrement de l'image dans le dossier
            $image->move(public_path('/images/'), $imageName);
        }

        return $imageName;
    }
}