<?php

namespace Tests\Feature;

use App\Enums\StatusEnum;
use App\Models\Profile;
use Tests\TestCase;

class ProfileTest extends TestCase
{
    /**
     * Récupération des profils en mode non authentifié
     */
    public function test_show_profile_without_status(): void
    {
        $response = $this->get('/api/profiles');

        // Récupératon des profils
        $profiles = json_decode($response->baseResponse->getContent(), 1);

        foreach ($profiles as $profile){
           $this->assertFalse(array_key_exists('status', $profile), 'Profiles shouldn\'t have status attribute');
        }
    }

    /**
     * Création du token
     */
    private function createToken() : string
    {
        $credentials = [
            'email' => 'a@a.a', 
            'password' => 'admin123' 
        ];

        $response = $this->post('/api/token/create', $credentials);

        // Récupération du token
        return json_decode($response->baseResponse->getContent(), 1)['token'];
    }

    /**
     * Récupération des profils en mode authentifié
     */
    public function test_show_profile_with_status(): void
    {
        $token = $this->createToken();
        
        $response = $this->get('/api/profiles', ['Authorization'=> 'Bearer ' . $token]);

        $profiles = json_decode($response->baseResponse->getContent(), 1);

        foreach ($profiles as $profile){
            $this->assertTrue(array_key_exists('status', $profile), "Profiles should have status attribute");
        }
    }
    
    /**
     * Création d'un profil
     */
    public function test_create_profile(): void
    {
        $token = $this->createToken();
                
        $newProfile = [
            'lastname' => fake()->lastName(),
            'firstname' => fake()->firstName(),
            'image'   => null,
            'status' => fake()->randomElement(StatusEnum::cases())->value
        ];

        $this->post('/api/profiles', $newProfile, ['Authorization'=> 'Bearer '.$token]);

        /**
         * @var Profile
         */
        $getTheLastProfile = Profile::latest('id')->first();
        $getTheLastProfile = $getTheLastProfile->getAttributes();
        
        $this->assertEqualsCanonicalizing($newProfile, array_intersect($newProfile, $getTheLastProfile), "Profile should be created");
    }
}
