<?php

namespace App\Enums;

enum StatusEnum: string
{
    case ACTIF = 'actif';
    case INACTIF = 'inactif';
    case EN_ATTENTE = 'en attente';
}