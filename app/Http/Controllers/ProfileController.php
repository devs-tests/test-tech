<?php

namespace App\Http\Controllers;

use App\Enums\StatusEnum;
use App\Http\Dto\CreateProfileDto;
use App\Http\Requests\CreateProfileRequest;
use App\Models\Profile;
use App\Services\ImageService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Collection<Profile>
     */
    public function index() : Collection
    {
        $params = ['lastname', 'firstname', 'image'];
        
        // Si on est authentifié, alors on affiche le status
        if (Auth::id()) {
            $params[] = 'status';
        };
        
        return Profile::select(...$params)
                ->where('status', StatusEnum::ACTIF)
                ->get();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateProfileRequest $request) : JsonResponse
    {
        $datas = new CreateProfileDto(
            $request->input('lastname'),
            $request->input('firstname'),
            $request->file('image'),
            $request->input('status')
        );

        try{
            $imageName = ImageService::storeImage($datas->image);

            Profile::create([
                ...(array) $datas,
                'image' => $imageName
            ]);

            return response()->json([
                'success' => 'The profile has been created successfully !'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage()
            ], 422);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CreateProfileRequest $request, string $id) : JsonResponse
    {
        $datas = new CreateProfileDto(
            $request->input('lastname'),
            $request->input('firstname'),
            $request->file('image'),
            $request->input('status')
        );

        try {
            $imageName = ImageService::storeImage($datas->image);

            $profile = Profile::firstWhere('id', $id);

            if ($profile) {
                assert($profile instanceof Profile);

                $profile->update([
                    ...(array) $datas,
                    'image' => $imageName
                ]);

                return response()->json([
                    'success' => 'The profile has been updated successfully !'
                ], 200);
            } else {
                return response()->json([
                    'error' => 'No profile found with id ' . $id . '!'
                ], 400);
            }
        } catch(\Exception $e){
            return response()->json([
                'error' => $e->getMessage()
            ], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id) : JsonResponse
    {
        $profile = Profile::firstWhere('id', $id);

        try {
            if ($profile) {
                assert($profile instanceof Profile);

                $profile->delete();

                return response()->json([
                    'success' => 'The profile has been deleted successfully !'
                ], 200);
            } else {
                return response()->json([
                    'error' => 'No profile found with id ' . $id . ' !'
                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage()
            ], 500);
        }
    }
}
