<?php

namespace App\Http\Controllers;

use App\Http\Dto\CreateTokenDto;
use App\Http\Requests\CreateTokenRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;

class AuthenticationController extends Controller
{
    public function createToken(CreateTokenRequest $request) : JsonResponse
    {
        $credentials = new CreateTokenDto(
            $request->input('email'),
            $request->input('password')
        );
        
        try {
            $user = User::firstWhere('email', $credentials->email);
            
            // Si aucun utilisateur OU mdp erroné
            if (!$user || !Hash::check($credentials->password, $user->password)) {
                return response()->json([
                    'error' => 'UNAUTHORIZED ! Invalid login or password'
                ], 401);
            }

            assert($user instanceof User);
            
            $token = $user->createToken($user->id.$user->email);
    
            return response()->json([
                'success' => 'The token has been created successfully !',
                'token' => $token->plainTextToken
            ], 200);
        } catch(\Exception $e){
            return response()->json([
                'error' => $e->getMessage()
            ], 422);
        }
    }
}
