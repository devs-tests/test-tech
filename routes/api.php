<?php

use App\Http\Controllers\AuthenticationController;
use App\Http\Controllers\ProfileController;
use App\Http\Middleware\OptionalAuthenticationSanctum;
use Illuminate\Support\Facades\Route;

// Création du token
Route::post('/token/create', [AuthenticationController::class, 'createToken']);

// Route pour la gestion des profils en mode authentifié
Route::resource('profiles', ProfileController::class)->except('index')->middleware('auth:sanctum');

// Route pour l'affichage des profils en mode public
Route::resource('profiles', ProfileController::class)->only('index')->middleware(OptionalAuthenticationSanctum::class);
