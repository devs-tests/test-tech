# Technos
LARAVEL 11
PHP 8.3.0
WAMP
SQLITE

# Pré-requis
Composer > version 2
PHP > version 8.2.0

# Installation du projet
- Cloner le projet via HTTPS
```sh
git clone https://gitlab.com/devs-tests/test-tech.git
cd test-tech
```

- Copier le .env.example
```sh
cp .env.example .env
```

- Lancer composer i
```sh
composer i
```

- Lancer les migrations
```sh
php artisan migrate
```

- Lancer les seeds
```sh
php artisan db:seed
```

- Lancer le serve
```sh
php artisan serve
```

- Lancer les tests
```sh
php artisan test
```

# Pour tester via postman
https://www.postman.com/atchiiii/workspace/test-tech

- Installer l'appli postman
- Survoler le bouton Send pour forker le workspace : Requests
- Commencer par la requête createToken afin de pouvoir créer un token et l'ajouter dans le header Authorization en Bearer token